# CMake generated Testfile for 
# Source directory: D:/pvpgn1.99.0/source/src
# Build directory: D:/pvpgn1.99.0/build/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(compat)
SUBDIRS(common)
SUBDIRS(win32)
SUBDIRS(tinycdb)
SUBDIRS(bntrackd)
SUBDIRS(client)
SUBDIRS(bniutils)
SUBDIRS(bnpass)
SUBDIRS(bnetd)
SUBDIRS(d2cs)
SUBDIRS(d2dbs)
SUBDIRS(test)
