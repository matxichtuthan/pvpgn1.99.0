# Install script for directory: D:/pvpgn1.99.0/source/src

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("D:/pvpgn1.99.0/build/src/compat/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/common/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/win32/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/tinycdb/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/bntrackd/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/client/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/bniutils/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/bnpass/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/bnetd/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/d2cs/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/d2dbs/cmake_install.cmake")
  INCLUDE("D:/pvpgn1.99.0/build/src/test/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

