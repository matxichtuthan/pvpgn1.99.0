/*
 * Copyright (C) 2000,2001	Onlyer	(onlyer@263.net)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "common/setup_before.h"
#include "setup.h"

#include <cerrno>
#include <cstdio>
#include <cstring>
#include <sstream>

#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif
#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#ifdef WIN32
# include "win32/service.h"
#endif
#ifdef WIN32_GUI
# include "win32/winmain.h"
#endif

#include "compat/stdfileno.h"
#include "compat/pgetpid.h"
#include "common/eventlog.h"
#include "common/xalloc.h"
#include "common/trans.h"
#include "common/fdwatch.h"
#include "prefs.h"
#include "connection.h"
#include "d2gs.h"
#include "serverqueue.h"
#include "d2ladder.h"
#include "cmdline.h"
#include "game.h"
#include "server.h"
#include "version.h"
#include "common/setup_after.h"

using namespace pvpgn::d2cs;
using namespace pvpgn;

#ifdef WIN32
char serviceLongName[] = "d2cs service";
char serviceName[] = "d2cs";
char serviceDescription[] = "Diablo 2 Character Server";

int g_ServiceStatus = -1;
#endif

static int init(void);
static int cleanup(void);
static int config_init(int argc, char * * argv);
static int config_cleanup(void);
static int setup_daemon(void);
static char * write_to_pidfile(void);


#ifdef DO_DAEMONIZE
static int setup_daemon(void)
{
	int pid;

	if (chdir("/")<0) {
		eventlog(eventlog_level_error,__FUNCTION__,"can not change working directory to root directory (chdir: %s)",std::strerror(errno));
		return -1;
	}
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
	if (!cmdline_get_foreground()) {
		close(STDERR_FILENO);
	}
	switch ((pid = fork())) {
		case 0:
			break;
		case -1:
			eventlog(eventlog_level_error,__FUNCTION__,"error create child process (fork: %s)",std::strerror(errno));
			return -1;
		default:
			return pid;
	}
	umask(0);
	setsid();
	return 0;
}
#endif


static char * write_to_pidfile(void)
{
	char *pidfile = xstrdup(prefs_get_pidfile());

	if (pidfile[0]=='\0') {
		xfree((void *)pidfile); /* avoid warning */
		return NULL;
	}

	if (pidfile) {
#ifdef HAVE_GETPID
		std::FILE * fp;

		if (!(fp = std::fopen(pidfile,"w"))) {
			eventlog(eventlog_level_error,__FUNCTION__,"unable to open pid file \"%s\" for writing (std::fopen: %s)",pidfile,std::strerror(errno));
			xfree((void *)pidfile); /* avoid warning */
			return NULL;
		} else {
			std::fprintf(fp,"%u",(unsigned int)getpid());
			if (std::fclose(fp)<0)
				eventlog(eventlog_level_error,__FUNCTION__,"could not close pid file \"%s\" after writing (std::fclose: %s)",pidfile,std::strerror(errno));
		}

#else
		eventlog(eventlog_level_warn,__FUNCTION__,"no getpid() std::system call, disable pid file in d2cs.conf");
		xfree((void *)pidfile); /* avoid warning */
		return NULL;
#endif
	}

	return pidfile;
}


static int init(void)
{
	d2cs_connlist_create();
	d2cs_gamelist_create();
	sqlist_create();
	d2gslist_create();
	gqlist_create();
	d2ladder_init();
	if(trans_load(d2cs_prefs_get_transfile(),TRANS_D2CS)<0)
	    eventlog(eventlog_level_error,__FUNCTION__,"could not load trans list");
	fdwatch_init(prefs_get_max_connections());
	return 0;
}


static int cleanup(void)
{
	d2ladder_destroy();
	d2cs_connlist_destroy();
	d2cs_gamelist_destroy();
	sqlist_destroy();
	d2gslist_destroy();
	gqlist_destroy();
	trans_unload();
	fdwatch_close();
	return 0;
}


static int config_init(int argc, char * * argv)
{
    char const * levels;
    char *       temp;
    char const * tok;
    int		 pid;

	if (cmdline_load(argc, argv) != 1) {
		return -1;
	}

#ifdef DO_DAEMONIZE
	if ((!cmdline_get_foreground())) {
		if (!((pid = setup_daemon()) == 0)) {
			return pid;
		}
	}
#endif

	if (d2cs_prefs_load(cmdline_get_preffile())<0) {
		eventlog(eventlog_level_error,__FUNCTION__,"error loading configuration file %s",cmdline_get_preffile());
		return -1;
	}

    eventlog_clear_level();
    if ((levels = d2cs_prefs_get_loglevels()))
    {
        temp = xstrdup(levels);
        tok = std::strtok(temp,","); /* std::strtok modifies the string it is passed */

        while (tok)
        {
        if (eventlog_add_level(tok)<0)
            eventlog(eventlog_level_error,__FUNCTION__,"could not add std::log level \"%s\"",tok);
        tok = std::strtok(NULL,",");
        }

        xfree(temp);
    }

#ifdef WIN32_GUI
	if (cmdline_get_gui()){
		eventlog_add_level(eventlog_get_levelname_str(eventlog_level_gui));
	}
#endif

#ifdef DO_DAEMONIZE
	if (cmdline_get_foreground()) {
		eventlog_set(stderr);
	}
	else
#endif
	{
	    if (cmdline_get_logfile()) {
		if (eventlog_open(cmdline_get_logfile())<0) {
			eventlog(eventlog_level_error,__FUNCTION__,"error open eventlog file %s",cmdline_get_logfile());
			return -1;
		}
	    } else {
		if (eventlog_open(d2cs_prefs_get_logfile())<0) {
			eventlog(eventlog_level_error,__FUNCTION__,"error open eventlog file %s",d2cs_prefs_get_logfile());
			return -1;
		}
	    }
	}
	return 0;
}


static int config_cleanup(void)
{
	d2cs_prefs_unload();
	cmdline_unload();
	return 0;
}

#include <direct.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <string>
#include <iostream>
using namespace std;

void CreateFolder(string path)
{
	if (!CreateDirectory(path.c_str(), NULL))
	{
		return;
	}
}

void RegWriteString(const char * key, const char* name, string value)
{
	HKEY hKey;
	memset(&hKey, 0, sizeof(hKey));

	if (RegOpenKeyEx(HKEY_CURRENT_USER, key, 0, KEY_WRITE, &hKey) == ERROR_SUCCESS)
		goto LABEL_1;
	else
	{
		RegCreateKeyEx(HKEY_CURRENT_USER, key, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
		goto LABEL_1;
	}
LABEL_1:
	RegSetValueEx(hKey, name, NULL, REG_SZ, (BYTE*)value.c_str(), value.length());
	RegCloseKey(hKey);
}

string ExePath() 
{
	char buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	string::size_type pos = string(buffer).find_last_of("\\/");
	return string(buffer).substr(0, pos);
}

void PrintD2gsList(char* Msg...)
{
	va_list arguments;
	va_start(arguments, Msg);

	int len = _vscprintf(Msg, arguments) + 1;
	char * text = new char[len];
	vsprintf_s(text, len, Msg, arguments);
	va_end(arguments);

	FILE* plik;
	fopen_s(&plik, "IpD2GS.ini", "a");
	if (plik)
	{
		fprintf(plik, "%s\n", text);
		fclose(plik);
	}
	delete[] text;
}

vector<D2GSConfig> D2GSArray;
void LoadConfigD2gslist()
{
	PrintD2gsList("[Ip D2GS]");
	int d2gslist = 1;
	char *D2GSL1 = 0, *D2GSL2 = 0;
	for (D2GSL2 = strtok_s(strdup(prefs_get_d2gs_list()), ", ", &D2GSL1); D2GSL2; D2GSL2 = strtok_s(NULL, ", ", &D2GSL1))
	{
		PrintD2gsList("%d = %s", d2gslist, D2GSL2);
		d2gslist++;
	}

	string V_ConfigAdmin;
	char filename[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, filename);
	V_ConfigAdmin.assign(filename);
	V_ConfigAdmin += "\\IpD2GS.ini";

	char szConfig[200];

	for (int aNumber = 1;; aNumber++)
	{
		ostringstream szNumber;
		szNumber << aNumber;

		GetPrivateProfileString("Ip D2GS", szNumber.str().c_str(), "", szConfig, 200, V_ConfigAdmin.c_str());
		if (szConfig[0] == '\0') break;

		string Config(szConfig);

		D2GSConfig hD2GSConfig;
		hD2GSConfig.id = szNumber.str();
		hD2GSConfig.Ip = Config;
		D2GSArray.push_back(hD2GSConfig);
	}
}

#ifdef WIN32_GUI
extern int app_main(int argc, char ** argv)
#else
extern int main(int argc, char ** argv)
#endif
{
	int pid;
	char * pidfile;

	eventlog_set(stderr);
	if (!((pid = config_init(argc, argv)) == 0)) {
//		if (pid==1) pid=0;
		return pid;
	}

	if (DeleteFileA("IpD2GS.ini"))
		LoadConfigD2gslist();
	else
		LoadConfigD2gslist();

	pidfile = write_to_pidfile();
	eventlog(eventlog_level_info,__FUNCTION__,D2CS_VERSION);
	if (init()<0) {
		eventlog(eventlog_level_error,__FUNCTION__,"failed to init");
		return -1;
	} else {
		eventlog(eventlog_level_info,__FUNCTION__,"server initialized");
	}
	if (d2cs_server_process()<0) {
		eventlog(eventlog_level_error,__FUNCTION__,"failed to run server");
		return -1;
	}
	cleanup();
	if (pidfile) {
		if (std::remove(pidfile)<0)
			eventlog(eventlog_level_error,__FUNCTION__,"could not remove pid file \"%s\" (std::remove: %s)",pidfile,std::strerror(errno));
		xfree((void *)pidfile); /* avoid warning */
	}
	config_cleanup();
	eventlog_close();
	return 0;
}
